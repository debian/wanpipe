/* wan_mem_debug.h */
#ifndef __WAN_MEMDEBUG_H_
#define __WAN_MEMDEBUG_H_

int sdla_memdbg_free(void);
int sdla_memdbg_init(void);

#endif
